require_relative 'i_carte_repository'
module AltTable 
    module OrganisationCarte
        module Repository
            

            class CarteRepository  < ICarteRepository

                def recuperation_liste_plats_dispo
                    begin
                        return  AltTable::OrganisationCarte::Models::Plat.all.where(:quantite.gt => 0)
                    end   
                end
            end
        end
    end
end