require_relative 'i_commande_repository'

module AltTable 
    module GestionCommandes
   
        module Repository


            class CommandeRepository < ICommandeRepository

                def restaurant_existe_par_nom? nom
                    begin
                        restaurant = AltTable::GestionCommandes::Models::Restaurant.find_by(nom: nom)
                        return true
                    rescue Mongoid::Errors::DocumentNotFound 
                        return false
                    end
                end

                def restaurant_a_un_service? nom
                    begin
                        restaurant = AltTable::GestionCommandes::Models::Restaurant.find_by(nom: nom)
                        return restaurant.service_date != nil
                    end
                end

                
                def recuperation_plan_table_service nom
                    begin
                        restaurant = AltTable::GestionCommandes::Models::Restaurant.find_by(nom: nom)
                        service_date = restaurant.service_date
                        service = AltTable::GestionCommandes::Models::Service.find_by(date: service_date)
                        return service.plan_table
                    end
                end

                def verifications_quantite_plat plats, nom_restaurant
                    
                    erreur_quantite = Array.new
                    plats.each do |plat|
                        plat_exist = AltTable::OrganisationCarte::Models::Plat.where(nom: plat[:nom]).exists?
                        return "NOTFOUND" if !plat_exist
                        plat_data = AltTable::OrganisationCarte::Models::Plat.find_by(nom: plat[:nom])
                        if plat[:quantite] > plat_data[:quantite] then
                            erreur_quantite.push(plat_data)
                        end
                    end
                    return erreur_quantite
                end

                def mise_a_jour_quantite_plats plats, nom_restaurant
                    plats.each do |plat|
                        plat_a_modifier = AltTable::OrganisationCarte::Models::Plat.find_by(nom: plat[:nom])
                        plat_a_modifier.update_attributes!(
                            quantite: plat_a_modifier[:quantite]-plat[:quantite]
                        )
                        plat_a_modifier.save!
                    end
                    return "UPDATED"
                end

                def mise_a_jour_plats_table nom_restaurant,nouveau_plan_table
                    puts nouveau_plan_table.to_json
                    restaurant = AltTable::GestionCommandes::Models::Restaurant.find_by(nom: nom_restaurant)
                    service_date = restaurant.service_date
                    service = AltTable::GestionCommandes::Models::Service.find_by(date: service_date)
                    service.update_attributes!(
                        plan_table: nouveau_plan_table
                    )
                    service.save!
                end

                

               
            end
        end
    end
end

