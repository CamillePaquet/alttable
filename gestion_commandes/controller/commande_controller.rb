include AltTable::GestionCommandes::Models
include AltTable::GestionCommandes::Services
include AltTable::GestionCommandes::Repository

AltTable::Backend.namespace '/altTable/api' do

  post '/commande/:nomRestaurant' do |nomRestaurant|
    body = request.body.read
    data = JSON.parse(body)
    plats_hash = Array::new
    data["plats"].each do |plat|
      plats_hash << {
        :nom => plat["nom"],
        :quantite => plat["quantite"],
        :commentaires => plat["commentaires"]
      }
    end
    options_commande = {
      :numero => data["numero"],
      :nom_restaurant => nomRestaurant,
      :plats => plats_hash
    }
    commande = Commande.new(options_commande)
    repo = CommandeRepository.new()
    commande_service = CommandeService.new(repo)
    response = commande_service.commander_service commande
    puts response.to_json
    if response.is_a?(Array) then
      status STATUS_MAP["BAD_REQUEST"][:status]
      return response.to_json
    end
    status STATUS_MAP[response][:status]
    return STATUS_MAP[response][:message]
  end



end