require_relative 'types/enum_type'
include AltTable::GestionCommandes::Enum

module AltTable
    module GestionCommandes
        module Services

            class CommandeService
                
                def initialize repo
                    @repo = repo
                end

                def commander_service  commande
                    return "NOTFOUND" if !@repo.restaurant_existe_par_nom? commande.nom_restaurant
                    return "BAD_REQUEST" if !@repo.restaurant_a_un_service? commande.nom_restaurant
                    plan_table = @repo.recuperation_plan_table_service commande.nom_restaurant
                    return "BAD_REQUEST" if !table_exist? plan_table,commande.numero
                    return "BAD_REQUEST" if clients_egal_zero plan_table,commande.numero
                    verification_quantite = @repo.verifications_quantite_plat(commande.plats, commande.nom_restaurant)
                    return verification_quantite if !verification_quantite.empty?
                    options_mise_a_jour_plan_table = {
                        :plan_table => plan_table,
                        :numero => commande.numero,
                        :nom_restaurant => commande.nom_restaurant,
                        :plats => commande.plats
                    }

                    mise_a_jour_plats_plan_table options_mise_a_jour_plan_table
                    return @repo.mise_a_jour_quantite_plats commande.plats, commande.nom_restaurant                   
                end

                def table_exist? plan_table, numero
                    plan_table.each do |table|
                        if table[:numero]  == numero then
                            return true 
                        end
                    end
                    return false
                end

                def clients_egal_zero plan_table, numero
                    plan_table.each do |table|
                        if table[:numero] == numero then
                            return true if table[:clients] == 0
                        end
                    end
                    return false
                end

                def mise_a_jour_plats_plan_table options={}
                    options[:plan_table].each do |table|
                        if table[:numero] == options[:numero] then
                            if table[:plats] == [] then
                                table[:plats]=options[:plats]
                            else
                                options[:plats].each do |plat_a_ajouter|
                                    plat_trouve = false
                                    table[:plats].each do |plat_present|
                                        if plat_present[:nom] == plat_a_ajouter[:nom] then
                                            plat_present[:quantite]+=plat_a_ajouter[:quantite]
                                            plat_present[:commentaires].concat(plat_a_ajouter[:commentaires])
                                            plat_trouve = true
                                        end
                                    end
                                    if !plat_trouve then
                                
                                        table[:plats].append(plat_a_ajouter)
                                    end
                                end
                            end
                        end
                    end
                    @repo.mise_a_jour_plats_table options[:nom_restaurant], options[:plan_table]
                end

            end
        end
    end
end

