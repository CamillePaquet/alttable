module AltTable
    module GestionCommandes
        module Enum
            TYPES = {
                :aperitif => "aperitif",
                :entree => "entree",
                :platPrincipal => "plat principal",
                :dessert => "dessert",
                :boisson => "boisson"
            }
        end
    end
end