class Commande
    attr_accessor :plats, :numero, :nom_restaurant
    
    def initialize(options)
        @plats = options[:plats]
        @numero = options[:numero]
        @nom_restaurant = options[:nom_restaurant]
    end

end